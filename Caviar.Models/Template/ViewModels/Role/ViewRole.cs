using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using Caviar.Models;
/// <summary>
/// 生成者：admin
/// 生成时间：1/7/2021 下午 2:36:25
/// 代码由代码生成器自动生成，更改的代码可能被进行替换
/// 可在上层目录使用partial关键字进行扩展
/// </summary>
namespace Caviar.Models
{
    [DisplayName("角色模型")]
    public partial class ViewRole:SysRole, IViewMode
    {
    }
}

﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Caviar.Models
{
    public class TabItem
    {
        public string KeyName { get; set; }
        public string TabName { get; set; }
        public string Content { get; set; }
    }
}

using Caviar.Core;
using Caviar.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Caviar.Models;
/// <summary>
/// 生成者：admin
/// 生成时间：7/7/2021 下午 5:46:07
/// 代码由代码生成器自动生成，更改的代码可能被进行替换
/// 可在上层目录使用partial关键字进行扩展
/// </summary>
namespace Caviar.Core.Enclosure
{
    [DisplayName("附件控制器")]
    public partial class EnclosureController : TemplateBaseController<EnclosureAction,SysEnclosure,ViewEnclosure>
    {
        
    }
}
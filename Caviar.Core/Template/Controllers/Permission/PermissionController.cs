using Caviar.Core;
using Caviar.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Caviar.Models;
/// <summary>
/// 生成者：admin
/// 生成时间：2021/6/16 14:25:53
/// 代码由代码生成器自动生成，更改的代码可能被进行替换
/// 可在上层目录使用partial关键字进行扩展
/// </summary>
namespace Caviar.Core.Permission
{
    [DisplayName("权限控制器")]
    public partial class PermissionController : TemplateBaseController<PermissionAction,SysPermission,ViewPermission>
    {
        
    }
}